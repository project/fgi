Field Group Image
Original author: Caleb Thorne (draenen) <caleb@calebthorne.com>
Date: Feb 2015

DESCRIPTION
------------

The Field Group Image module allows uploading an image to a field group that
will be displayed in the group on form or display pages.

REQUIREMENTS
------------

This module requires the following modules:
 * Field (core)
 * Image (core)
 * Field Group (https://drupal.org/project/field_group)

CONFIGURATION
-------------

To add an image to a field group:
* Add a new field group
* Edit the field group settings to upload an image and select an image style.
